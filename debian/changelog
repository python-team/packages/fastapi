fastapi (0.101.0-2) unstable; urgency=medium

  [ Stefano Rivera ]
  * Patch: Fix test_warn_duplicate_operation_id. (Closes #1054764)

 -- Sandro Tosi <morph@debian.org>  Sun, 14 Jan 2024 02:18:21 -0500

fastapi (0.101.0-1) unstable; urgency=medium

  * New upstream release

 -- Sandro Tosi <morph@debian.org>  Thu, 10 Aug 2023 13:56:04 -0400

fastapi (0.100.0-2) unstable; urgency=medium

  * run autopkgtests via autopkgtest-pkg-pybuild, refactoring how we teach
    d/rules to run tests

 -- Sandro Tosi <morph@debian.org>  Mon, 31 Jul 2023 21:33:21 -0400

fastapi (0.100.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - add dirty-equals to b-d, needed by test
    - add typing-extensions to b-d, needed by tests
  * debian/rules
    - skip test_dependency_gets_exception

 -- Sandro Tosi <morph@debian.org>  Mon, 24 Jul 2023 01:14:13 -0400

fastapi (0.92.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - bump b-d on starlette to >= 0.25.0

 -- Sandro Tosi <morph@debian.org>  Fri, 24 Feb 2023 16:15:44 -0500

fastapi (0.91.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - bump b-d on starlette to >= 0.24.0

 -- Sandro Tosi <morph@debian.org>  Mon, 13 Feb 2023 23:33:51 -0500

fastapi (0.89.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - bump Standards-Version to 4.6.2.0 (no changes needed)
  * debian/copyright
    - extend packaging copyright years
  * debian/tests/unittests
    - run autopkgtest on all supported python versions

 -- Sandro Tosi <morph@debian.org>  Sun, 22 Jan 2023 02:53:46 -0500

fastapi (0.88.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - bump httpx b-d to >= 0.23.1, needed for python 3.11 compatibility;
      Closes: #1025242
    - bump b-d on starlette to >= 0.22.0
    - add trio to b-d, needed by tests
  * debian/rules
    - ignore DeprecationWarnins during pytest

 -- Sandro Tosi <morph@debian.org>  Fri, 09 Dec 2022 16:38:27 -0500

fastapi (0.85.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Security-Contact.

  [ Sandro Tosi ]
  * debian/control
    - tighten b-d on starlette to >= 0.20.4

 -- Sandro Tosi <morph@debian.org>  Thu, 29 Sep 2022 22:02:40 -0400

fastapi (0.85.0-1) unstable; urgency=medium

  * New upstream release; Closes: #1020083
  * Drop patches, no longer needed
  * debian/control
    - build system, switch from flit to hatchling
  * skip more tests during build and autopkgtests

 -- Sandro Tosi <morph@debian.org>  Tue, 27 Sep 2022 23:24:21 -0400

fastapi (0.74.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sandro Tosi ]
  * New upstream release
  * debian/control
    - remove pytest-asyncio from b-d, not needed
  * debian/patches/PR4483.patch
    - support starlette 0.18.0; Closes: #1005547, #1005635

 -- Sandro Tosi <morph@debian.org>  Sat, 26 Feb 2022 22:32:18 -0500

fastapi (0.73.0-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - extend packaging copyright years
  * debian/rules debian/tests/unittests
    - skip more tests depending on jose, not available in debian yet

 -- Sandro Tosi <morph@debian.org>  Tue, 25 Jan 2022 23:38:13 -0500

fastapi (0.70.1-1) unstable; urgency=medium

  * New upstream release
  * debian/tests
    - add autopkgtest
  * debian/control
    - bump Standards-Version to 4.6.0.1 (no changes needed)
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Mon, 20 Dec 2021 01:29:58 -0500

fastapi (0.70.0-2) unstable; urgency=medium

  * use flit build backend instead of a custom setup.py file

 -- Sandro Tosi <morph@debian.org>  Mon, 13 Dec 2021 23:52:49 -0500

fastapi (0.70.0-1) unstable; urgency=medium

  * New upstream release
    - fixes CVE-2021-32677, Closes: #990582
  * debian/patches
    - drop patches, merged upstream
  * debian/control
    - tighen up starlette b-d version
  * debian/rules
    - exclude 2 more tests, which need an updated httpx
  * debian/control
    - tighen starlette and pydantic versions, add yaml to b-d

 -- Sandro Tosi <morph@debian.org>  Fri, 08 Oct 2021 22:35:10 -0400

fastapi (0.63.0-2) unstable; urgency=medium

  * debian/watch
    - track github releases

 -- Sandro Tosi <morph@debian.org>  Sun, 31 Jan 2021 18:29:02 -0500

fastapi (0.63.0-1) unstable; urgency=low

  * Initial release; Closes: #977492

 -- Sandro Tosi <morph@debian.org>  Sun, 10 Jan 2021 23:22:17 -0500
